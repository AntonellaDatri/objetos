package equipoDeTrabajo;

import static org.junit.Assert.*;

import java.time.LocalDate;
import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

public class EquipoDeTrabajoTest {
	Persona juana;
	Persona juan;
	Persona pedro;
	EquipoDeTrabajo empresa;
	ArrayList<Persona> equipo;
	@Before 
	public void setUp() throws Exception {
		juana = new Persona("Juana", LocalDate.of(1999, 12, 4));
		juan = new Persona("Juan", LocalDate.of(1969, 9, 25));
		pedro = new Persona("Pedro", LocalDate.of(2000, 1, 14));
		empresa = new EquipoDeTrabajo("Empresa1");
		
		empresa.addEmpleado(juan);
		empresa.addEmpleado(juana);
		empresa.addEmpleado(pedro);
	}
	
	@Test
	public void oipo() {
		assertEquals(empresa.promedioEdad(), 29);
	}
	
	

}
