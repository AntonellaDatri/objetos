package multioperador;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import multioperado.Multioperador;

public class MultioperadorTest {
	public Multioperador multioperador;
	@Before
	public void setUp() throws Exception {
	//Se crea el contador
		multioperador = new Multioperador();

	 //Se agregan los numeros. Un solo par y nueve impares
		multioperador.addNumber(1);
		multioperador.addNumber(3);
		multioperador.addNumber(5);
		multioperador.addNumber(7);
		multioperador.addNumber(9);
		multioperador.addNumber(1);
		multioperador.addNumber(1);
		multioperador.addNumber(1);
		multioperador.addNumber(1);
		multioperador.addNumber(4);
	}
	
	@Test
	public void sumaDeLista() {
		assertEquals(multioperador.sumaDeLista(), 33);
	}
	@Test
	public void restaDeLista() {
		assertEquals(multioperador.restaDeLista(), -33);
	}
	@Test
	public void multiplicacionDeLista() {
		assertEquals(multioperador.multiplicaciónDeLista(), 3780);
	}
}
