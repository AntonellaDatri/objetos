package point;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class PointTest {
	Point point;
	Point point00;
	@Before
	public void setUp() throws Exception {
		point = new Point(3,4);
		point00 = new Point();
	}
	@Test
	public void actualPosition() {
		assertEquals(point.actualPosition(),"(3,4)");
	}
	@Test
	public void posicionActualDebeSer00() {
		assertEquals(point00.actualPosition(),"(0,0)");
	}
	@Test
	public void cambioDePunto() {
		point.newPosition(7,8);
		assertEquals(point.actualPosition(),"(7,8)");
	}
	@Test
	public void sumaDePuntos() {
		point.sumPoint(5,6);
		assertEquals(point.actualPosition(),"(8,10)");
	}
}
