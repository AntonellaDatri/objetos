package encapsulamiento;

import static org.junit.Assert.*;

import java.time.LocalDate;

import org.junit.Before;
import org.junit.Test;

public class EncapsulamientoTest {
	Persona juana;
	Persona juan;
	@Before
	public void setUp() throws Exception {
		juana = new Persona("Juana", LocalDate.of(1999, 12, 04));
		juan = new Persona("Juan", LocalDate.of(1969, 9, 15));
	}	
	
	@Test
	public void edad() {
		assertEquals(juana.edad(), 19);
	}
	@Test
	public void menorQue() {
		assertEquals(juana.menorQue(juan), true);
		assertEquals(juan.menorQue(juana), false);
	}

}
