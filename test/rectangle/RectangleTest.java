package rectangle;

import static org.junit.Assert.*;
import point.Point;
import org.junit.Before;
import org.junit.Test;

public class RectangleTest {
	Rectangle rectangulo;
	Rectangle rectangulo2;
	
	@Before
	public void setUp() throws Exception {
		rectangulo = new Rectangle(5,3);
		rectangulo2 = new Rectangle(new Point(2,1),5,3);
		}
	
	@Test
	public void puntoDeInicio() {
		assertEquals (rectangulo2.getStartingPoint() , "(2,1)");
	}
	@Test
	public void area() {
		assertEquals (rectangulo2.area() , 15);
	}
	@Test
	public void perimetro() {
		assertEquals (rectangulo2.perimeter() , 16);
	}
	@Test
	public void esHorizontal() {
		assertEquals (rectangulo2.itsVertical() , false);
	}
	@Test
	public void esVertical() {
		assertEquals (rectangulo2.itsHorizontal() , true);
	}

}
