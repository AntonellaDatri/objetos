package multioperado;

import java.util.ArrayList;

public class Multioperador {
	ArrayList <Integer> number = new ArrayList<Integer>();

	public ArrayList<Integer> getNumber() {
		return number;
	}
	public void setNumber(ArrayList<Integer> number) {
		this.number = number;
	}
	public void addNumber(int n) {
		number.add(n);
	}

	
	public int sumaDeLista() {
		int suma = 0;
		for (int numero : number ) {
		 suma += numero;
		}
		return suma;
		
	}
	public int restaDeLista() {
		int suma = 0;
		for (int numero : number ) {
		 suma -= numero;
		}
		return suma;
	}
	public int multiplicaciónDeLista() {
		int suma = 1;
		for (int numero : number ) {
		 suma *= numero;
		}
		return suma;
	}
	
	
	
	
}
