package rectangle;
import point.Point; 

public class Rectangle {
	Point startingPoint;
	int base;
	int height;
	
	public String getStartingPoint() {
		return startingPoint.actualPosition();
	}

	public void setStartingPoint(Point startingPoint) {
		this.startingPoint = startingPoint;
	}

	public int getBase() {
		return base;
	}

	public void setBase(int base) {
		this.base = base;
	}

	public int getHeight() {
		return height ;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public Rectangle(Point startingPoint, int base, int height) {
		super();
		this.startingPoint = startingPoint;
		this.base = base;
		this.height = height;
	}
	
	public Rectangle(int base, int height) {
		super();
		this.base = base;
		this.height = height;
	}
	public String size() {
		Point B = new Point((startingPoint.getX() + base), startingPoint.getY());
		Point C = new Point((base + startingPoint.getX()), (height + startingPoint.getY()));
		Point D = new Point(startingPoint.getX(), (height + startingPoint.getY()));
		return startingPoint.actualPosition() + " " + B.actualPosition() + " " + C.actualPosition() + " " + D.actualPosition();
	}
	public int area() {
		return base * height;
	}
	public int perimeter() {
		return (base * 2) + (height * 2);
	}
	public Boolean itsVertical(){
		return this.height > this.base;
	}
	public Boolean itsHorizontal() {
		return !(this.itsVertical());
	}
}
