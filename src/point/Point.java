package point;

public class Point {
	int y = 0;
	int x = 0;
	public int getY() {
		return y;
	}
	public void setY(int y) {
		this.y = y;
	}
	public int getX() {
		return x;
	}
	public void setX(int x) {
		this.x = x;
	}
	public Point(int x, int y) {
		super();
		this.x = x;
		this.y = y;
		
	}
	public Point() {
		super();
	}
	
	public void newPosition(int _x, int _y) {
		x = _x;
		y = _y;
	}
	
	public void sumPoint(int _x, int _y) {
		x += _x;
		y += _y;
	}
	
	public String actualPosition() {
		return "("+ x + "," + y + ")";
	}
	
	
}
