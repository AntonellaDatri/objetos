package equipoDeTrabajo;

import java.util.ArrayList;

public class EquipoDeTrabajo {
	String nombre;
	ArrayList<Persona> empleados = new ArrayList<Persona>();
	
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public ArrayList<Persona> getEmpleados() {
		return empleados;
	}
	public void setEmpleados(ArrayList<Persona> empleados) {
		this.empleados = empleados;
	}
	public void addEmpleado(Persona empleado) {
		empleados.add(empleado);
	}
	public int promedioEdad() {
		int promedio = 0;
		for (Persona empleado: empleados) {
		 promedio += empleado.edad();
		}
		promedio = promedio / empleados.size();
		
		return promedio;
		
	}
	
	public EquipoDeTrabajo(String nombre, ArrayList<Persona> empleados) {
		super();
		this.nombre = nombre;
		this.empleados = empleados;
	}
	public EquipoDeTrabajo(String nombre) {
		super();
		this.nombre = nombre;
	}
	
	
	
}
