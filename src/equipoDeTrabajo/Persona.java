package equipoDeTrabajo;

import java.time.LocalDate;

public class Persona {
	String nombre;
	String Apellido;
	LocalDate fechaDeNacimiento;
	
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public LocalDate getFechaDeNacimiento() {
		return fechaDeNacimiento;
	}
	public void setFechaDeNacimiento(LocalDate fechaDeNacimiento) {
		this.fechaDeNacimiento = fechaDeNacimiento;
	}
	
	public int edad(){
		LocalDate fechaActual = LocalDate.now();
		if (fechaDeNacimiento.getDayOfYear() < fechaActual.getDayOfYear()) {
			return fechaActual.getYear() - fechaDeNacimiento.getYear();
		}
		else {return fechaActual.getYear() - fechaDeNacimiento.getYear() -1;}
	}
	public Persona(String nombre, LocalDate fechaDeNacimiento) {
		super();
		this.nombre = nombre;
		this.fechaDeNacimiento = fechaDeNacimiento;
	}
	public Boolean menorQue(Persona persona) {
		return persona.edad() > this.edad();
	}
	
}
