package unq;

import java.util.ArrayList;

public class Counter {
	private ArrayList <Integer> number = new ArrayList<Integer>();
	
	
	public ArrayList<Integer> getNumber() {
		return number;
	}

	public void setNumber(ArrayList<Integer> number) {
		this.number = number;
	}
	
	public void addNumber(int n) {
		number.add(n);
	}

	public int cantidadDePares() {
		int suma = 0;
		for (int entero:number ) {
		 suma += this.unoSiEsPar(entero);
		}
		return suma;
	}
	private int unoSiEsPar(int entero) {
		if (this.esPar(entero)) {
			return 1;
		}
		else {return 0;}
	}


	private boolean esPar(int entero) {
		return (entero % 2) == 0;
	}


	public int cantidadDeImpares() {
		int suma = 0;
		for (int entero:number ) {
		 suma += this.unoSiEsImpar(entero);
		}
		return suma;
	}
	private int unoSiEsImpar(int entero) {
		if (this.esImpar(entero)) {
			return 1;
		}
		else {return 0;}
	}


	private boolean esImpar(int entero) {
		return (entero % 2) == 1;
	}


	public int cantidadDeMúltiplosDe_(int n) {
		int suma = 0;
		for (int entero:number ) {
		 suma += this.unoSiEsMultiploDe(n,entero);
		}
		return suma;
	}	


	private int unoSiEsMultiploDe(int n, int entero) {
		if ((entero % n) == 0) {
			return 1;
		}
		else {return 0;}

	}

	public Counter() {}

}
